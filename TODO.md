- zoom viewport
- add stop button
- refactor Renderer to split into renderer and generator classes?
- draw grids in debug mode
- randomly-generated 'megabuildings'
	- put buildings on top of each other
- group tall buildings together?
	- make height a function of nearby buildings as well
- draw the roads differently
	- ideally, some sort of gradient so that the edges of the road are lighter
	- but then we can't draw the roads one by one, because they'll overlap and (maybe?) break
	  the effect
- profile the city generation code
	- what's slower -- generating the blocks or drawing them?
		- if drawing is slower, then not sure how to make better...can't batch drawing
		  together because order matters