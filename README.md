#PixelCity - Isometric city generator

Cross-platform iso pixel city generator program.

![Screenshot 1](img/prog_1.PNG)

##Dependencies:

| Package | Version |
|---------|---------|
| Python  | 3.4.5   |
| Pillow  | 3.4.2   |
| PyQt5   | 5.7     |

##Guide

This program will draw pretty buildings for you. Hit 'Update' to apply colour changes to the *currently-generated* city. 'Regenerate' will scrap the city and generate a new one using the current parameters. Export your image to PNG using the 'Save' button.

The building length/width/height parameters follow a [Pareto distribution](https://en.wikipedia.org/wiki/Pareto_distribution). Lowering alpha increases the variability.

Road width is determined by selecting a uniformly random number from the interval *[average_width - variance, average_width + variance)*.

##Known Issues

- it's slow.
- items aren't always rendered in the correct order
- (Windows) color picker background is set to the selected color, instead of the usual widget background. I think this is a Windows+PyQt5 issue.

##Images

![Screenshot 2](img/prog_2.png)

![Screenshot 3](img/prog_3.PNG)

![Example image](img/example.png)

![](img/desert.png)

![](img/cyber.png)

![](img/zoom.png)