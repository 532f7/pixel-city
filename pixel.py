#!/usr/bin/env python3
from PIL import Image, ImageDraw, ImageQt
import sys, random, math
from PyQt5.QtCore import QThread, pyqtSignal, Qt
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import (QWidget, QApplication, QSizePolicy, QLayout, QStyleFactory)
from PyQt5.QtGui import QPixmap, QGuiApplication, QPalette, QColor

class Renderer(QThread):
	# signals
	finished = pyqtSignal(Image.Image)
	progress = pyqtSignal(str, int)

	# properties
	regenerate = True

	def run(self):
		if self.regenerate:
			im = self.generate()
		else:
			im = self.new_image()
			self.render(im, self.blocks, self.roads)
		self.finished.emit(im)

	def world_to_screen(self, coords):
		return ((coords[0] - coords[1])*2, coords[0] + coords[1])

	def draw_block(self, im, block):
		draw = ImageDraw.Draw(im)

		import colorsys
		import math
		import random

		def tint(color, max_tint, darken=False):
			hls = colorsys.rgb_to_hls(color[0]/255, color[1]/255, color[2]/255)

			tint = random.randint(-1 * max_tint, max_tint) / 255
			rgb = colorsys.hls_to_rgb(
				hls[0] + tint, hls[1]*(1 if not darken else 0.2), hls[2])
			return tuple(math.floor(255*x) for x in rgb)

		fill_l, fill_r, fill_t = [tint(x, config['color']['hue_shift_max'])
								  for x in [config['color']['lo'], config['color']['mid'], config['color']['hi']]]

		# coordinates
		def add(one, two):
			return tuple(sum(x) for x in zip(one, two))

		if config["debug"]:
			lighten = min(255, math.floor((block.h / config['building']['height']['avg'] / 3) * 255))
			draw.rectangle([block.coords, add(block.coords, (block.w, -block.l))],
				fill=(255,lighten,lighten), outline = (128,0,0))
			return

		screen_coords = self.world_to_screen(block.coords)
		bottom_left = add((0, -1 * block.w), screen_coords)
		bottom_middle = add(bottom_left, (block.w*2 + 1, block.w))
		bottom_right = add(bottom_middle, (2 * block.l + 1, -1 * block.l))

		top_left = add((0, -1 * block.h), bottom_left)
		top_upper_middle = add((block.l*2 + 1, -1 * block.l), top_left)
		top_right = add(top_upper_middle, (block.w*2 + 1, block.w))
		top_lower_middle = add(top_left, (block.w*2 + 1, block.w))

		# L
		draw.polygon([top_left, top_lower_middle, bottom_middle, bottom_left],
					 outline=config['color']['outline'],
					 fill=fill_l)
		# R
		draw.polygon([top_lower_middle, top_right, bottom_right, bottom_middle],
					 outline=config['color']['outline'],
					 fill=fill_r)
		# top
		draw.polygon([top_left, top_upper_middle, top_right, top_lower_middle],
					 outline=config['color']['outline'],
					 fill=fill_t)

		# lights
		def window_pixels(origin, length, is_left_side):
			i = 0
			is_second_pixel = False
			while i < length - 1:
				x = origin[0] + 2*i + (1 if is_second_pixel else 0)
				y = origin[1] + i * (1 if is_left_side else -1)
				yield (x, y)

				if is_second_pixel:
					i = i + 1
					is_second_pixel = False
				else:
					is_second_pixel = True

		def is_window_on():
			return random.randint(0, 100) > config['building']['percentage_lights_off']

		l_on = tint(config['color']['lights']['l'], 0, False)
		l_off = tint(config['color']['lights']['l'], 0, True)
		r_on = tint(config['color']['lights']['r'], 0, False)
		r_off = tint(config['color']['lights']['r'], 0, True)

		num_rows = block.h//2 - 1
		for y in range(1, num_rows, 2):
			for pixel in window_pixels(add(top_left, (2, 3 + 2*y)), block.w, True):
				color = l_on if is_window_on() else l_off
				draw.point(pixel, color)

			for pixel in window_pixels(add(top_lower_middle, (2, 1 + 2*y)), block.l, False):
				color = r_on if is_window_on() else r_off
				draw.point(pixel, color)


	def new_image(self):
		im = Image.new('RGBA', (config['size']['width'], config['size']['height']), config['color']['bg'])
		return im;

	def generate(self):
		# helpers
		def rand(dimension):
			import math
			import random
			return math.ceil(random.paretovariate(dimension['alpha']) * dimension['avg'])

		def gen_roads(num_roads, avg_width, var, xrange, yrange):
			ret = []
			for i in range(0, num_roads):
				x = random.randint(xrange[0], xrange[1])
				y = random.randint(yrange[0], yrange[1])
				width = random.randint(avg_width - var, avg_width + var)
				is_horiz = random.randint(0, 1)

				if width > avg_width:
					length = math.ceil(max(0, width - avg_width) / var * xrange[1])
				else:
					length = random.randint(10, xrange[1]//2)

				r = Road(x, y, length, width, is_horiz)
				ret.append(r)
			return ret

		def is_on_road(block, roads):
			num_skipped = 0
			x, y = block.coords

			for road in roads:
				if x + block.w < road.x or x > road.x + (road.len if road.is_horiz else road.width):
					continue

				if y < road.y or y - block.l > road.y + (road.width if road.is_horiz else road.len):
					continue

				return True

			return False

		# models
		class Block(object):
			def __init__(self, l, w, h, coords):
				super(Block, self).__init__()
				self.l = l
				self.w = w
				self.h = h
				self.coords = coords

		class Road(object):
			def __init__(self, x, y, len, width, is_horiz):
				super(Road, self).__init__()
				self.x = x
				self.y = y
				self.len = len
				self.width = width
				self.is_horiz = is_horiz

		im = self.new_image()

		world_left_edge = -10
		world_right_edge = int((im.width / 2 + im.height) / 2)
		world_bottom_edge = int((im.height - (0 / 2)) / 2)
		world_top_edge = int((0 - im.width/2) / 2)

		blocks = []
		roads = gen_roads(config['roads']['limit'],
					config['roads']['width'],
					config['roads']['var'],
					(world_left_edge + 10, world_right_edge - 10),
					(world_top_edge + 10, world_bottom_edge - 10))

		num_generated = 0
		for y in range(world_top_edge, world_bottom_edge, config['building']['spacing']['y']):
			x = world_left_edge
			while x < world_right_edge:
				l = rand(config['building']['length'])
				w = rand(config['building']['width'])
				h = rand(config['building']['height'])

				offset = (x, y)
				block = Block(l, w, h, offset)

				if not is_on_road(block, roads):
					num_generated = num_generated + 1
					if num_generated % 100 == 0:
						self.progress.emit("Generated %d" % num_generated, 0)
					blocks.append(block)

				x = x + w + config['building']['spacing']['x']
		self.progress.emit("Generated %d" % num_generated, 0)

		self.blocks = blocks
		self.roads = roads

		return self.render(im, blocks, roads)

	def render(self, image, blocks, roads):
		def draw_grid(im, space):
			draw = ImageDraw.Draw(im)
			for y in range(-1 * im.height, im.height, space):
				draw.line(
					[(0, y), (im.width, y + im.width / 2)], fill=config['color']['grid'], width=1)
			for y in range(-1 * im.height, im.height, space):
				draw.line(
					[(im.width, y), (0, y + im.width / 2)], fill=config['color']['grid'], width=1)

		def draw_roads(im, roads):
			road_img = Image.new('RGBA', im.size)
			d = ImageDraw.Draw(road_img)
			for road in roads:
				color = tuple(random.randint(50, 150) for x in range(0, 3))

				if config["debug"]:
					if road.is_horiz:
						d.rectangle([road.x, road.y, road.x + road.len, road.y + road.width],
							fill=(0,0,255),
							outline=(0,255,255))
					else:
						d.rectangle([road.x, road.y, road.x + road.width, road.y + road.len],
							fill=(0,0,255),
							outline=(0,255,255))
				else:
					if road.is_horiz:
						start_world = (road.x, road.y + road.width // 2)
						end_world = (start_world[0] + road.len, start_world[1])
						
						d.line([self.world_to_screen(start_world), self.world_to_screen(end_world)],
							color,
							width = int(road.width*2.5))
					else:
						start_world = (road.x + road.width // 2, road.y)
						end_world = (start_world[0], start_world[1] + road.len)
						
						d.line([self.world_to_screen(start_world), self.world_to_screen(end_world)],
							color,
							width = int(road.width*2.5))
			im.paste(road_img, road_img)

		if config["roads"]["should_draw"]:
			draw_roads(image, roads)

		if not config["debug"] and config["grid"]:
			draw_grid(image, (config['building']['spacing']['x'] + config['building']['spacing']['y']))

		num_blocks = len(blocks)
		for idx, block in enumerate(sorted(blocks, key=lambda b: b.coords[1])):
			self.draw_block(image, block)
			if idx % 100 == 1:
				self.progress.emit("Drawn %d/%d" % (idx + 1, len(blocks)), math.ceil(100 * idx / num_blocks))
		self.progress.emit("Drawn %d/%d" % (num_blocks, num_blocks), 100)
		return image

def rgb(value):
	value = value.lstrip('#')
	lv = len(value)
	return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))

config = {
	"debug" : False,
	"grid" : True,
	"building" : {
		"spacing" : {
			"x" : 3,
			"y" : 3
		},
		"length" : {
			"avg" : 2,
			"alpha" : 5
		},
		"width" : {
			"avg" : 2,
			"alpha" : 5
		},
		"height" : {
			"avg" : 7,
			"alpha": 3
		},
		"percentage_lights_off" : 50
	},
	"roads" : {
		"var" : 3,
		"width" : 5,
		"limit" : 30,
		"should_draw" : True
	},
	"color" : {
		"bg": rgb("#141824"), ##### TODO: store these as hex strings and parse before rendering?
		"outline": rgb("#141824"),
		"hi": rgb("#2F4E53"),
		"mid": rgb("#061F33"),
		"lo": rgb("#020509"),
		"lights": {
			"l": rgb("#aCdCfF"),
			"r": rgb("#CCFCFF")
		},
		"grid" : (75, 75, 85),
		"hue_shift_max" : 15
	},
	"size" : {
		"width" : 400,
		"height": 400
	},
}

class BindingMixin(object):
	def __init__(self):
		super(BindingMixin, self).__init__()
		self.target = config

	def setBinding(self, binding):
		self.binding = binding.split(".")

	def updateProperty(self, value):
		self._updateProperty(self.binding, self.target, value)

	def _updateProperty(self, keys, target, value):
		if len(keys) == 1:
			target[keys[0]] = value
		else:
			self._updateProperty(keys[1:], target[keys[0]], value)

	def getProperty(self):
		return self._getProperty(self.binding, self.target)

	def _getProperty(self, keys, target):
		if len(keys) == 1:
			return target[keys[0]]
		else:
			return self._getProperty(keys[1:], target[keys[0]])

class BoundCheckbox(QtWidgets.QCheckBox, BindingMixin):
	def __init__(self, text, parent, binding):
		super(BoundCheckbox, self).__init__(text, parent)
		self.setBinding(binding)
		self.setChecked(self.getProperty())
		self.stateChanged.connect(self.updateProperty)

class BoundSpinner(QtWidgets.QSpinBox, BindingMixin):
	def __init__(self, parent, min, max, binding):
		super(BoundSpinner, self).__init__(parent)
		self.setBinding(binding)
		self.setRange(min, max)
		self.setValue(self.getProperty())
		self.valueChanged.connect(self.updateProperty)
		self.setFocusPolicy(Qt.StrongFocus)

	def wheelEvent(self, evt):
		if not self.hasFocus():
			evt.ignore()
		else:
			super(BoundSpinner, self).wheelEvent(evt)

class BoundColorButton(QtWidgets.QPushButton, BindingMixin):
	def __init__(self, parent, binding):
		super(BoundColorButton, self).__init__(parent)
		self.setBinding(binding)
		super(BoundColorButton, self).clicked.connect(self.clicked)
		self.update()

	def update(self):
		color = self.getProperty()
		self.setStyleSheet("background-color: rgb%s" % str(color))

	def clicked(self, e):
		initial_color = QColor(*self.getProperty())
		color = QtWidgets.QColorDialog.getColor(initial_color, parent = self)
		if color.isValid():
			rgb = color.rgb()
			self.updateProperty(((rgb & 0xff0000) >> 16, (rgb & 0xff00) >> 8, rgb & 0xff))
			self.update()

class AppWindow(QtWidgets.QMainWindow):

	def update_canvas(self):
		qim = ImageQt.ImageQt(self.image)
		pixmap = QPixmap.fromImage(qim)

		self.canvas.items()[0].setPixmap(pixmap)
		self.canvas_view.setSceneRect(0, 0, pixmap.width(), pixmap.height())

	def draw_overlay(self):
		# draw white overlay onto our image
		overlay_img = Image.new('RGBA', self.image.size)
		d = ImageDraw.Draw(overlay_img)
		d.rectangle([(0, 0), self.image.size], fill=(255, 255, 255, 64))
		self.image.paste(overlay_img, mask=overlay_img)
		self.update_canvas()
		self.enable_config(False)
		
	def redraw_clicked(self, e):
		self.draw_overlay()
		self.renderer.regenerate = True
		self.renderer.start()

	def update_clicked(self, e):
		self.draw_overlay()
		self.renderer.regenerate = False
		self.renderer.start()

	def debug_clicked(self, value):
		self.draw_overlay()
		config["debug"] = value
		self.renderer.regenerate = False
		self.renderer.start()

	def save_clicked(self, e):
		filename = QtWidgets.QFileDialog.getSaveFileName(self, caption = "Save Image",
			filter = "PNG Images (*.png)")
		if filename and filename[0]:
			try:
				self.image.save(filename[0])
			except Exception as e:
				err_dialog = QtWidgets.QErrorMessage(self)
				err_dialog.showMessage(str(e))

	def render_complete(self, image):
		self.image = image
		self.update_canvas()
		self.enable_config(True)

	def update_progress(self, value, percentage):
		self.statusBar().showMessage(value)
		self.progress_bar.setValue(percentage)

	def create_labeled_spinner(self, text, binding, min, max, parent):
		container = QtWidgets.QFrame(parent)
		container_layout = QtWidgets.QHBoxLayout()
		container_layout.setContentsMargins(0, 0, 0, 0)
		container.setLayout(container_layout)

		label = QtWidgets.QLabel(text, container)
		label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
		container_layout.addWidget(label)

		spinner = BoundSpinner(container, min, max, binding)
		label.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Preferred)
		container_layout.addWidget(spinner)

		container.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)

		parent.layout().addWidget(container)

	def create_color_picker(self, text, binding, parent):
		container = QtWidgets.QFrame(parent)
		container_layout = QtWidgets.QHBoxLayout(container)
		container_layout.setContentsMargins(0,0,0,0)

		label = QtWidgets.QLabel(text, container)
		label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
		container_layout.addWidget(label)

		picker = BoundColorButton(container, binding)
		picker.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Preferred)
		container_layout.addWidget(picker)

		parent.layout().addWidget(container)

	def create_header_label(self, text, parent):
		lbl = QtWidgets.QLabel(text, parent)
		font = QGuiApplication.instance().font()
		font.setBold(True)
		lbl.setFont(font)
		parent.layout().addWidget(lbl)

	def enable_config(self, value):
		self.configpane.setEnabled(value)

	def __init__(self, parent=None):
		super(AppWindow, self).__init__(parent)
		# import random
		# random.seed(1)

		self.setWindowTitle("PixelCity")

		root_frame = QtWidgets.QFrame(self)
		root_frame.setContentsMargins(0,0,0,0)
		self.setCentralWidget(root_frame)
		root_layout = QtWidgets.QHBoxLayout(root_frame)

		self.statusBar()

		scrollArea = QtWidgets.QScrollArea(root_frame)
		scrollArea.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
		scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
		root_layout.addWidget(scrollArea)

		configpane = QtWidgets.QFrame()
		self.configpane = configpane
		configpane.setContentsMargins(0,0,10,0) # scroll bar overlaps...
		configlayout = QtWidgets.QVBoxLayout(configpane)
		configlayout.setAlignment(Qt.AlignTop)

		button_container = QtWidgets.QFrame(configpane)
		button_container_layout = QtWidgets.QHBoxLayout(button_container)
		button_container_layout.setContentsMargins(0,0,0,0)
		configlayout.addWidget(button_container)

		save_button = QtWidgets.QPushButton("Save...", configpane)
		save_button.clicked.connect(self.save_clicked)
		configlayout.addWidget(save_button)

		update_button = QtWidgets.QPushButton("Update", button_container)
		update_button.clicked.connect(self.update_clicked)
		button_container_layout.addWidget(update_button)

		redraw_button = QtWidgets.QPushButton("Regenerate", button_container)
		redraw_button.clicked.connect(self.redraw_clicked)
		button_container_layout.addWidget(redraw_button)

		self.create_header_label("Image", configpane)
		self.create_labeled_spinner("Width", "size.width", 100, 10000, configpane)
		self.create_labeled_spinner("Height", "size.height", 100, 10000, configpane)

		self.create_header_label("Buildings", configpane)
		self.create_labeled_spinner("Spacing (x)", "building.spacing.x", 1, 1000, configpane)
		self.create_labeled_spinner("Spacing (y)", "building.spacing.y", 1, 1000, configpane)
		self.create_labeled_spinner("Length (average)", "building.length.avg", 2, 1000, configpane)
		self.create_labeled_spinner("Length (alpha)", "building.length.alpha", 1, 1000, configpane)
		self.create_labeled_spinner("Width (average)", "building.width.avg", 2, 1000, configpane)
		self.create_labeled_spinner("Width (alpha)", "building.width.alpha", 1, 1000, configpane)
		self.create_labeled_spinner("Height (average)", "building.height.avg", 3, 1000, configpane)
		self.create_labeled_spinner("Height (alpha)", "building.height.alpha", 1, 1000, configpane)
		self.create_labeled_spinner("% lights off", "building.percentage_lights_off", 0, 100, configpane)

		self.create_header_label("Roads", configpane)
		self.create_labeled_spinner("Width (average)", "roads.width", 1, 1000, configpane)
		self.create_labeled_spinner("Variance", "roads.var", 1, 1000, configpane)
		self.create_labeled_spinner("Limit", "roads.limit", 0, 1000, configpane)

		self.create_header_label("Colour", configpane)
		self.create_color_picker("Background", "color.bg", configpane)
		self.create_color_picker("Outline", "color.outline", configpane)
		self.create_color_picker("Top", "color.hi", configpane)
		self.create_color_picker("Right", "color.mid", configpane)
		self.create_color_picker("Left", "color.lo", configpane)
		self.create_color_picker("Lights (L)", "color.lights.l", configpane)
		self.create_color_picker("Lights (R)", "color.lights.r", configpane)
		configlayout.addWidget(BoundCheckbox("Coloured roads", configpane, "roads.should_draw"))

		grid_container = QtWidgets.QFrame(self)
		grid_container_layout = QtWidgets.QHBoxLayout(grid_container)
		grid_container_layout.setContentsMargins(0,0,0,0)
		grid_container_layout.addWidget(BoundCheckbox("Grid", grid_container, "grid"))
		self.create_color_picker("", "color.grid", grid_container)
		configlayout.addWidget(grid_container)

		self.create_labeled_spinner("Max hue shift", "color.hue_shift_max", 0, 255, configpane)

		debug_checkbox = QtWidgets.QCheckBox("Debug", configpane)
		debug_checkbox.stateChanged.connect(self.debug_clicked)
		configlayout.addWidget(debug_checkbox)

		scrollArea.setWidget(configpane)

		self.canvas = QtWidgets.QGraphicsScene(self)
		self.canvas_view = QtWidgets.QGraphicsView(self.canvas)
		self.canvas_view.setDragMode(QtWidgets.QGraphicsView.ScrollHandDrag)
		self.canvas.addPixmap(QPixmap())
		self.canvas_view.setSceneRect(0, 0, config['size']['width'] + 35, config['size']['height'] + 35)
		root_layout.addWidget(self.canvas_view)

		self.progress_bar = QtWidgets.QProgressBar(self.statusBar())
		self.progress_bar.setRange(0, 100)
		self.progress_bar.setTextVisible(False)
		self.progress_bar.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
		self.progress_bar.setAlignment(Qt.AlignLeft)
		self.statusBar().addPermanentWidget(self.progress_bar)

		self.renderer = Renderer()
		self.renderer.finished.connect(self.render_complete)
		self.renderer.progress.connect(self.update_progress)
		self.renderer.start()
		self.enable_config(False)
		self.show()

if __name__ == '__main__':
	qapp = QApplication(sys.argv)

	qapp.setStyle(QStyleFactory.create("fusion"))
	palette = QPalette()
	palette.setColor(QPalette.Window, QColor(53,53,53));
	palette.setColor(QPalette.WindowText, Qt.white);
	palette.setColor(QPalette.Disabled, QPalette.WindowText, QColor(150,150,150));
	palette.setColor(QPalette.Base, QColor(15,15,15));
	palette.setColor(QPalette.AlternateBase, QColor(53,53,53));
	palette.setColor(QPalette.ToolTipBase, Qt.white);
	palette.setColor(QPalette.ToolTipText, Qt.white);
	palette.setColor(QPalette.Text, QColor(240,240,240));
	palette.setColor(QPalette.Disabled, QPalette.Text, QColor(150,150,150));
	palette.setColor(QPalette.Button, QColor(53,53,53));
	palette.setColor(QPalette.Disabled, QPalette.Button, QColor(25,25,25));
	palette.setColor(QPalette.ButtonText, Qt.white);
	palette.setColor(QPalette.Disabled, QPalette.ButtonText, QColor(128,128,128))
	palette.setColor(QPalette.BrightText, Qt.red);

	palette.setColor(QPalette.Highlight, QColor(229,159,0));
	palette.setColor(QPalette.HighlightedText, Qt.black);
	qapp.setPalette(palette);

	main = AppWindow()
	sys.exit(qapp.exec_())
